#'Tea Time
#'
#'\code{teatime} Does Tea Time stuff
#'
#'This function reads in an x value or array and returns the values squared. It also talks to you.
#'
#' @param x value or array that is inputted
#' @param person name of the person you want to embarass
#'
#' @return the values of x^2
#'
#' @examples
#' input a set of x values and the name of a person
#' y = teatime(2, "Roger")
#' @export teatime


teatime = function(x, person){

  # Use the print function
  print(paste(person, ", I thought there would be Tea at 'Tea Time'?", sep = ""))


  # Lets solve an equation as well so we can return it
  y = x^2


  return(y)
}
