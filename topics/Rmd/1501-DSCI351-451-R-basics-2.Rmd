---
title: "CWRU DSCI351-451: Practicum: R-basics-2"
author: "Prof.:Roger French, TA:Yang Hu"
date: "January 13, 2015"
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

<!--
# Script Name: 1501cwru-dsci-351-451-R-basics.Rmd
# Purpose: This is a a quick intro to R basics  
# Authors: Roger H. French
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########
# Latest Changelog Entires:
# v0.00.02 - 1501cwru-dsci-351-451-R-basics.Rmd - RF building on Jenny Bryan's intro
##########

# Rmd code goes below the comment marker!
-->

## Simple Rmd rules

- Rcode in Rmd, delineated by three ticks{r}

```{r,echo=TRUE,message=TRUE}
options("digits"=5)
options("digits.secs"=3)
```
 
- Code in Rmd is delineated 
-- three ticks for code blocks
-- one tick for inline code

tick is a back tilted apostrophe
Not a apostrophe!

# R has a mind-blowing collection of built-in functions 

-that are accessed like so

##functionName(arg1 = val1, arg2 = val2, and so on)

## Functions  

- Come from base R, 
- Or from R packages
- All the > 7000 R packages are in CRAN
[CRAN = The Comprehensive R Archive Network](http://cran.case.edu/)

>- And you'll write your own functions  

- from your own scripts

### Let’s try using seq() 

- seq() makes regular sequences of numbers and, 

While we’re at it, 

- demo more helpful features of RStudio.

### Type se and hit TAB. 

#### A pop up shows you possible completions. 

- Specify seq() by typing more to disambiguate 
- or using the up/down arrows to select. 

#### Notice the floating tool-tip-type help that pops up, 

- reminding you of a function’s arguments. 
- If you want even more help, 
- press F1 as directed 
- to get the full documentation in the help tab of the lower right pane. 

#### Now open the parentheses and 

- notice the automatic addition of the closing parenthesis and 
- the placement of cursor in the middle. 
- Type the arguments 1, 10 and hit return. 

#### RStudio also exits the parenthetical expression for you. 

```{r}
seq(1, 10)
````

### The above also demonstrates something about how R resolves function arguments. 

- You can always specify in name = value form. 
- But if you do not, R attempts to resolve by position. 
- So above, it is assumed that we want a sequence from = 1 that goes to = 1. 
- Since we didn’t specify step size, the default value of by in the function definition is used, which ends up being 1 in this case. 
- For functions I call often, I might use this resolve by position for the first argument or maybe the first two. 
- After that, I always use name = value.

## Make this assignment and notice similar help with quotation marks.

yo <- "hello world"

- If you just make an assignment, 
- you don’t get to see the value, 
- so then you’re tempted to immediately inspect.

```{r}
y <- seq(1, 10)
y
```

### This common action can be shortened 

- by surrounding the assignment with parentheses, 
- which causes assignment and “print to screen” to happen.

```{r}
(y <- seq(1, 10))
```

Not all functions have (or require) arguments:

```{r}
date()
## [1] "Mon Jan  5 14:20:26 2015"
```

## Now look at your workspace – in the upper right pane. 

The workspace is where user-defined objects accumulate. 

- You can also get a listing of these objects with commands:

objects()
## [1] "jenny_rocks"                "this_is_a_really_long_name"
## [3] "x"                          "y"                         
## [5] "yo"
ls()
## [1] "jenny_rocks"                "this_is_a_really_long_name"
## [3] "x"                          "y"                         
## [5] "yo"


### If you want to remove the object named y, you can do this

rm(y)

To remove everything:

rm(list = ls())

or click the broom in RStudio’s Environment pane.

## Workspace and working directory
One day you will need to quit R, 

- go do something else and 
- return to your analysis later.

One day you will have multiple analyses going that use R and 

- you want to keep them separate.

One day you will need to bring data from the outside world into R and 

- send numerical results and figures from R back out into the world.

To handle these real life situations, you need to make two decisions:

- What about your analysis is “real”, i.e. you will save it as your lasting record of what happened?

- Where does your analysis “live”?

Workspace, .RData

### As a beginning R user, it’s OK to consider your workspace “real”. 
Very soon, I urge you to evolve to the next level, 

- where you consider your saved R scripts as “real”. 

(In either case, of course 

- the input data is very much real and requires preservation!) 
- With the input data and the R code you used, 
- you can reproduce everything. 

#### You can make your analysis fancier. 

- You can get to the bottom of puzzling results and 
- discover and fix bugs in your code. 
- You can reuse the code to conduct similar analyses in new projects. 
- You can remake a figure with different aspect ratio 
- or save is as TIFF instead of PDF. 

#### You are ready to take questions. 

- You are ready for the future.

### If you regard your workspace as “real” 

- (saving and reloading all the time), 
- if you need to redo analysis … 
- you’re going to either redo a lot of typing (making mistakes all the way) 
- or will have to mine your R history for the commands you used. 

Rather than becoming an expert on managing the R history, 

- a better use of your time and psychic energy 
- is to keep your “good” R code 
- in a script for future reuse.

Because it can be useful sometimes, 

- note the commands you’ve recently run appear in the History pane.

But you don’t have to choose right now and the two strategies are not incompatible. 

### Let’s demo the save / reload the workspace approach.

#### Upon quitting R, you have to decide 

- if you want to save your workspace, 
- for potential restoration the next time you launch R. 

Depending on your set up, R or your IDE, eg RStudio, 

- will probably prompt you to make this decision.

#### Quit R/Rstudio, 

- either from the menu, using a keyboard shortcut, 
- or by typing q() in the Console. You’ll get a prompt like this:

Save workspace image to ~/.Rdata?

- Note where the workspace image is to be saved and then click Save.

Using your favorite method, visit the directory where image was saved and verify there is a file named .RData. You will also see a file .Rhistory, holding the commands submitted in your recent session.

#### Restart RStudio. 

- In the Console you will see a line like this:

[Workspace loaded from ~/.RData]

- indicating that your workspace has been restored. 
- Look in the Workspace pane and you’ll see the same objects as before. 
- In the History tab of the same pane, 
- you should also see your command history. 
- You’re back in business. 

#### This way of starting and stopping analytical work 

- will not serve you well for long but it’s a start.

## Working directory

Any process running on your computer has a notion of its “working directory”. 

- In R, this is where R will look, by default, 
- for files you ask it to load. 

It also where, by default, 

- any files you write to disk will go. 
- Chances are your current working directory 
- is the directory we inspected above, 
- i.e. the one where RStudio wanted to save the workspace.

You can explicitly check your working directory with:
```{r}
getwd()
```
It is also displayed at the top of the RStudio console.

### As a beginning R user, 

- it’s OK let your home directory 
- or any other weird directory on your computer 
- be R’s working directory. 

#### Very soon, I urge you to evolve to the next level, 

- where you organize your analytical projects into directories and, 
- when working on project A, set R’s working directory to the associated directory.

### setwd: Although I do not recommend it, 

- in case you’re curious, 
- you can set R’s working directory at the command line like so:

setwd("~/myCoolProject")

#### Although I do not recommend it, 

- you can also use RStudio’s Files pane 
- to navigate to a directory and 
- then set it as working directory from the menu: 
- Session –> Set Working Directory –> To Files Pane Location. 
- (You’ll see even more options there). 
Or within the Files pane, 
- choose More and Set As Working Directory.

### But there’s a better way. 

- A way that also puts you on the path 
- to managing your R work like an expert.

## RStudio projects
Keeping all the files associated with a project organized together

- input data, R scripts, analytical results, figures 
– is such a wise and common practice 
- that RStudio has built-in support for this via its projects.

### Using Projects

#### Let’s make one to use for the rest of this workshop/class. 
Do this: File > New Project …. 

- The directory name you choose here will be the project name. 
- Call it whatever you want (or follow me for convenience).

#### I created a directory and, 
therefore RStudio project, 

-called swc in my tmp directory, FYI.


setwd("~/tmp/swc")

Now check that the “home” directory for your project 

- is the working directory of our current R process:

getwd()

I can’t print my output here 

-because this document itself 
- does not reside in the RStudio Project we just created.

Let’s enter a few commands in the Console, as if we are just beginning a project:

```{r}
a <- 2
b <- -3
sig_sq <- 0.5
x <- runif(40)
y <- a + b * x + rnorm(40, sd = sqrt(sig_sq))
(avg_x <- mean(x))


write(avg_x, "avg_x.txt")
plot(x, y)
abline(a, b, col = "purple")


dev.print(pdf, "toy_line_plot.pdf")
```

### Let’s say this is a good start of an analysis and 

- your ready to start preserving the logic and code. 

#### Visit the History tab 

- of the upper right pane. 
- Select these commands. 
- Click “To Source”. 

#### Now you have a new pane containing a nascent R script. 

- Click on the floppy disk to save. 
- Give it a name ending in .R or .r, 
- I used toy-line.r and note that, 
- by default, it will go in the directory associated with your project.

#### Quit RStudio. 
Inspect the folder associated with your project if you wish. 

-Maybe view the PDF in an external viewer.

#### Restart RStudio. 
Notice that things, by default, 

- restore to where we were earlier, 
- e.g. objects in the workspace, 
- the command history, 
- which files are open for editing, 
- where we are in the file system browser, 
- the working directory for the R process, etc. 

#### These are all Good Things.

### Change some things about your code. 
Top priority would be to set a sample size n at the top, e.g. n <- 40, and 

- then replace all the hard-wired 40’s with n. 

Change some other minor-but-detectable stuff, 

- i.e. alter the sample size n, 
- the slope of the line b,the color of the line … whatever. 

#### Practice the different ways to re-run the code:

Walk through line by line by keyboard shortcut (command + enter) 

- or mouse (click Run in the upper right corner of editor pane).

Source the entire document 

– equivalent to entering source('toy-line.r') in the Console 
– by keyboard shortcut (shift command S) 
- or mouse (click Source in the upper right corner of editor pane or select from the mini-menu accessible from the associated down triangle).

- Source with echo from the Source mini-menu.

Visit your figure in an external viewer 

- to verify that the PDF is changing as you expect.

#### In your favorite OS-specific way, 

- search your files for toy_line_plot.pdf and 
- presumably you will find the PDF itself (no surprise) 
- but also the script that created it (toy-line.r). 

#### This latter phenomenon is a huge win. 

- One day you will want to remake a figure 
- or just simply understand where it came from. 

If you rigorously save figures to file 

- with R code 
- and not ever ever ever the mouse or the clipboard, 
- you will sing my praises one day. Trust me.

## stuff
It is traditional to save R scripts with a .R or .r suffix. 

- Follow this convention unless you have some extraordinary reason not to.
- Google R Style says filename.R   

### Comments in R code start with one or more # symbols. 

- Use them. 
- RStudio helps you (de)comment selected lines 
- with Ctrl+Shift+C (windows and linux) 
- or Command+Shift+C (mac).

### Clean out the workspace, 

- ie pretend like you’ve just revisited this project after a long absence. 
- The broom icon or rm(list = ls()). 
- Good idea to do this, restart R (available from the Session menu), 

### re-run your analysis 

- to truly check that the code you’re saving 
- is complete and correct (or at least rule out obvious problems!).

## This workflow will serve you well in the future:

#### Create an RStudio project for an analytical project from your Git repo
#### Keep inputs there (we’ll soon talk about importing)
#### Keep scripts there; edit them, run them in bits or as a whole from there
#### Keep outputs there (like the PDF written above)

## Avoid using the mouse for pieces of your analytical workflow, 

- such as loading a dataset \
- or saving a figure. 

### Terribly important for reproducability: "No Mousey Mousey"

- and for making it possible to retrospectively determine 
- how a numerical table or PDF 
- was actually produced (searching on local disk on filename, among .R files, will lead to the relevant script).

### Many long-time users never save the workspace, 

- never save .RData files (I’m one of them), 
- never save or consult the history. 

#### Once/if you get to that point, 

- there are options available in RStudio 
- to disable the loading of .RData 
- and permanently suppress the prompt on exit to save the workspace 
- (go to Tools->Options->General).

## For the record, when loading data into R and/or writing outputs to file, 

- you can always specify the absolute path 
- and thereby insulate yourself from the current working directory. 
- This is rarely necessary when using RStudio Projects properly.

# Links and License 
 
http://www.r-project.org 

http://rmarkdown.rstudio.com/  

http://stat545-ubc.github.io/block002_hello-r-workspace-wd-project.html

This work is licensed under the CC BY-NC 3.0 Creative Commons License.

<!--
# Keep a complete change log history at bottom of file.
# Complete Change Log History
# v0.00.01 - 1405-07 - Roger French made this markdown from Jenny Bryan's start
##########
Here are standard YAML Headers for Rmarkdown/Markdown/PanDoc
  1. Beamer slides and PDF Report, html
  2. html and PDF report as full long pages
  3.ioSlides slides and PDF report. ioSlides is buggy, not well supported, but looks nice. 

---
title: "1501-DSCI351-451-R-basics-2.Rmd"
author: Roger French
date: January 26, 2015
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  ioslides_presentation:
    toc: true
    self_contained: true
    smaller: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

-->